-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  ven. 13 mars 2020 à 10:27
-- Version du serveur :  10.3.16-MariaDB
-- Version de PHP :  7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `e-commerce`
--

-- --------------------------------------------------------

--
-- Structure de la table `adresse`
--

CREATE TABLE `adresse` (
  `id` int(11) NOT NULL,
  `user_id_id` int(11) DEFAULT NULL,
  `adresse` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contry` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `adresse`
--

INSERT INTO `adresse` (`id`, `user_id_id`, `adresse`, `city`, `post_code`, `contry`) VALUES
(12, 1, '5 rue d\'alger', 'nantes', '44444', 'France'),
(15, 2, '5 rue d\'alger', 'nantes', '44444', 'France');

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `categorie_id` int(11) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `specification` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `html_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `number_click` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `solde` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `article`
--

INSERT INTO `article` (`id`, `user_id`, `categorie_id`, `description`, `title`, `price`, `specification`, `html_description`, `stock`, `number_click`, `created_at`, `solde`) VALUES
(10, NULL, 6, 'Article description...Apple\'s Magic Mouse is the first multitouch mouse: its large touch surface.', 'Magic mouse', 85, '[{\"specificationTitle\":\"Interface\",\"specificationContent\":\"Bluetooth\"},{\"specificationTitle\":\"button on\\/off\",\"specificationContent\":\"yes\"},{\"specificationTitle\":\"weight\",\"specificationContent\":\"106g\"}]', NULL, 10, 3, '2020-02-03 14:07:13', NULL),
(11, NULL, 7, '\nThe Magic Keyboard is the keyboard that comes standard with all iMacs makes maximum advantage of.', 'Imac', 1999, '[{\"specificationTitle\":\"weight\",\"specificationContent\":\"5,66kg\"},{\"specificationTitle\":\"Processor\",\"specificationContent\":\"Intel Core i5-3470S\"},{\"specificationTitle\":\"stockage\",\"specificationContent\":\"8go\"}]', NULL, 29, 6, '2020-02-04 14:24:22', 1),
(12, NULL, 7, 'Descriptif :PC Tour HP 8200 Ecran 22\" Intel Core i3-2120 RAM 8\nModèle : HP 8200 Elite format MT Processe', 'Lenovo', 790, '[{\"specificationTitle\":\"Modele\",\"specificationContent\":\"HP 8200 Elite format MT\"},{\"specificationTitle\":\"Processor\",\"specificationContent\":\"ntel Core i3-2120 - 3.3 GHz - 2 coeurs \"},{\"specificationTitle\":\"weight\",\"specificationContent\":\"12kg\"}]', NULL, 1, 2, '2020-02-04 15:24:53', 25),
(13, NULL, 8, 'Laptop - HP ProBook 440 G6 - 14\" FHD - Core i5-8265U - RAM 8Go - Stockage 256Go SSD - Intel UHD Graphics 620 - Windows ', 'Hp ', 699, '[{\"specificationTitle\":\"Processor\",\"specificationContent\":\" Core i5-8265U\"},{\"specificationTitle\":\"RAM\",\"specificationContent\":\"8kg\"},{\"specificationTitle\":\"System d\'exploitation \",\"specificationContent\":\"Windows 10\"}]', NULL, 3, NULL, '2020-02-03 14:24:40', NULL),
(14, NULL, 6, 'The Aorus M3 is a wired mouse with a lightweight design, a 6400 dpi optical sensor and 7 programmable buttons', 'Mouse Gaming', 59, '[{\"specificationTitle\":\"weight\",\"specificationContent\":\"178g\"},{\"specificationTitle\":\"color\",\"specificationContent\":\"black\"},{\"specificationTitle\":\"bluetooth\",\"specificationContent\":\"no\"}]', NULL, 25, 1, '2020-02-03 14:28:43', 1),
(15, NULL, 9, 'Keyboard keys (buttons) typically have characters engraved or printed on them,source needed and each press of a key typically ', 'Keyboard gamer', 99, '[{\"specificationTitle\":\"weight\",\"specificationContent\":\"300g\"},{\"specificationTitle\":\"width\",\"specificationContent\":\"60cm\"},{\"specificationTitle\":\"height\",\"specificationContent\":\"30cm\"},{\"specificationTitle\":\"plop\",\"specificationContent\":\"test\"}]', NULL, 23, 7, '2020-02-04 15:23:23', 1),
(16, NULL, 8, 'It has several configurations, the basic version of which is an 8-core Intel Xeon processor 8-core Intel Xeon processor.', 'Macbook pro', 1299, '[{\"specificationTitle\":\"weight\",\"specificationContent\":\"1kg\"},{\"specificationTitle\":\"color\",\"specificationContent\":\"acier\"},{\"specificationTitle\":\"screen size\",\"specificationContent\":\"13\'\"}]', NULL, 99, 1, '2020-02-03 14:39:48', 1),
(17, NULL, 9, 'To use a keyboard shortcut, press and hold one or more modifier keys and then press the last key of the shortcut.', 'Mac keyboard', 100, '[{\"specificationTitle\":\"height\",\"specificationContent\":\"25cm\"},{\"specificationTitle\":\"width\",\"specificationContent\":\"35cm\"},{\"specificationTitle\":\"weight\",\"specificationContent\":\"200g\"}]', NULL, 35, NULL, '2020-02-03 14:39:31', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL,
  `categorie` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id`, `categorie`, `parent_id`) VALUES
(6, 'mouse', NULL),
(7, 'desktop', NULL),
(8, 'laptop', NULL),
(9, 'keyboard', NULL),
(11, 'joystick', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `credit_card`
--

CREATE TABLE `credit_card` (
  `id` int(11) NOT NULL,
  `user_id_id` int(11) DEFAULT NULL,
  `card_number` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `credit_card`
--

INSERT INTO `credit_card` (`id`, `user_id_id`, `card_number`, `expiration_date`) VALUES
(27, 1, '111111111111111', '3333-03-01'),
(31, 2, '111111111111111', '2222-02-01');

-- --------------------------------------------------------

--
-- Structure de la table `credit_cart`
--

CREATE TABLE `credit_cart` (
  `id` int(11) NOT NULL,
  `card_number` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id_id` int(11) NOT NULL,
  `articles` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `picture`
--

CREATE TABLE `picture` (
  `id` int(11) NOT NULL,
  `article_id_id` int(11) DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `picture`
--

INSERT INTO `picture` (`id`, `article_id_id`, `path`, `alt`) VALUES
(4, 10, 'magicmouse-5e3829113af63.jpeg', NULL),
(5, 11, 'imac-5e382a4ddfde4.jpeg', NULL),
(6, 12, 'lenovodesktop-5e382c545d94a.jpeg', NULL),
(7, 13, 'hpomen-5e382d282500a.jpeg', NULL),
(8, 14, 'gaminkeyboard-5e382e115a95a.jpeg', NULL),
(9, 15, 'hpkeyboard-5e382f0d0648e.jpeg', NULL),
(10, 16, 'macpro-5e3830426ac98.jpeg', NULL),
(11, 17, 'macboard-5e3830a3377c0.jpeg', NULL),
(14, 15, 'add_clavier-5e398c6bdef1d.jpeg', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_online` tinyint(1) NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`, `lastname`, `firstname`, `is_online`, `phone`) VALUES
(1, 'admin@admin.fr', '[\"ROLE_ADMIN\", \"ROLE_USER\"]', '$2y$13$hTCWDblX5vwI0i5w1MVQ7.1zj9S6Ignbtqc1B6ebl6DjAmBOjot3q', 'admin', 'admin', 1, '1234567898'),
(2, 'pinson.ewen@gmail.com', '[\"ROLE_ADMIN\", \"ROLE_USER\"]', '$2y$13$ZInsAdtacCCn/Tez/DnwPeDrBKyaYuFpSLxDUxxOqIi98/cJNkIjS', 'user', 'user', 1, '0000000000');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `adresse`
--
ALTER TABLE `adresse`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C35F08169D86650F` (`user_id_id`);

--
-- Index pour la table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_23A0E66A76ED395` (`user_id`),
  ADD KEY `IDX_23A0E66BCF5E72D` (`categorie_id`);

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `credit_card`
--
ALTER TABLE `credit_card`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_11D627EE9D86650F` (`user_id_id`);

--
-- Index pour la table `credit_cart`
--
ALTER TABLE `credit_cart`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F52993989D86650F` (`user_id_id`);

--
-- Index pour la table `picture`
--
ALTER TABLE `picture`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_16DB4F898F3EC46` (`article_id_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `adresse`
--
ALTER TABLE `adresse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `credit_card`
--
ALTER TABLE `credit_card`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT pour la table `credit_cart`
--
ALTER TABLE `credit_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `picture`
--
ALTER TABLE `picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `adresse`
--
ALTER TABLE `adresse`
  ADD CONSTRAINT `FK_C35F08169D86650F` FOREIGN KEY (`user_id_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `FK_23A0E66A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_23A0E66BCF5E72D` FOREIGN KEY (`categorie_id`) REFERENCES `categorie` (`id`);

--
-- Contraintes pour la table `credit_card`
--
ALTER TABLE `credit_card`
  ADD CONSTRAINT `FK_11D627EE9D86650F` FOREIGN KEY (`user_id_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `FK_F52993989D86650F` FOREIGN KEY (`user_id_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `picture`
--
ALTER TABLE `picture`
  ADD CONSTRAINT `FK_16DB4F898F3EC46` FOREIGN KEY (`article_id_id`) REFERENCES `article` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
