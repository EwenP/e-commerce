<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    /**
     * find article by it's category
     * @return Article[]
     */
    public function findByCategorie($categorie_id): array
    {
        return $this->createQueryBuilder('a')
                    ->andWhere('a.categorie = :categorie')
                    ->setParameter('categorie', $categorie_id)
                    ->orderBy('a.price', 'ASC')
                    ->getQuery()
                    ->getResult();
    }


    public function searchArticle($param)
    {
      $param = '%' . $param . '%';
      $rawSql = "SELECT * from article WHERE title LIKE :param ORDER BY number_click ASC";
      $stmt = $this->getEntityManager()->getConnection()->prepare($rawSql);
      $stmt->execute(['param' => $param]);
      // dd($stmt->fetchAll());
      return $stmt->fetchAll();
    }


}
