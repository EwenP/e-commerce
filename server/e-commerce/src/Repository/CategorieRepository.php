<?php

namespace App\Repository;

use App\Entity\Categorie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Categories|null find($id, $lockMode = null, $lockVersion = null)
 * @method Categories|null findOneBy(array $criteria, array $orderBy = null)
 * @method Categories[]    findAll()
 * @method Categories[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategorieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Categorie::class);
    }

    /**
     * @return Categories[]
     */
    public function findArticleByCategory()
    {
        $qb = $this->createQueryBuilder('p')
            ->orderBy('p.categorie', 'ASC');

        $query = $qb->getQuery();

        return $query->execute();
    }

    public function findCategory($categorie) {
      $rawSql = "SELECT * from categorie WHERE categorie = :categorie";
      $stmt = $this->getEntityManager()->getConnection()->prepare($rawSql);
      $stmt->execute([':categorie' => $categorie]);
      return $stmt->fetchAll();
      // dd($stmt->fetchAll());
    }
}
