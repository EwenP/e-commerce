<?php

namespace App\Repository;

use App\Entity\CreditCart;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CreditCart|null find($id, $lockMode = null, $lockVersion = null)
 * @method CreditCart|null findOneBy(array $criteria, array $orderBy = null)
 * @method CreditCart[]    findAll()
 * @method CreditCart[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CreditCartRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CreditCart::class);
    }
}
