<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Parser;
use App\Repository\UserRepository;


class TestblaController extends AbstractController
{
    /**
     * @Route("/test", name="test")
     */
    public function index(Request $request, UserRepository $userRepository)
    {
      $token = $request->headers->get('token');
      $msg = $userRepository->isAuth($token);
      
        $ret = new Response(json_encode($msg));
        $ret->headers->set('Content-Type', 'application-json');
        $ret->headers->set('Access-Control-Allow-Origin', '*');
        $ret->headers->set('Access-Control-Allow-Methods', '*');
        return $ret;
    }
}
