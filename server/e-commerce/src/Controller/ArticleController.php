<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Picture;
use App\Entity\Categorie;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use App\Repository\PictureRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use App\Repository\CategorieRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * @Route("/article")
 */
class ArticleController extends AbstractController
{

    /**
     * Find Article by Categories
     * @Route("/categorie/{categorie_id}", name="article_by_category", methods={"GET"})
     */
    public function findFilter($categorie_id, ArticleRepository $categorieRepository):Response
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $articles = $categorieRepository->findByCategorie($categorie_id);
        $jsonContent = $serializer->serialize($articles, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);
        $article = new Response($jsonContent);
        $article->headers->set('Content-Type', 'application-json');
        $article->headers->set('Access-Control-Allow-Origin', '*');
        $article->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS');
        return $article;
    }

    /**
     * Fetch All Articles
     * @Route("/", name="article_index", methods={"GET"})
     */
    public function index(ArticleRepository $articleRepository): Response
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $all_articles = $articleRepository->findBy(array(), array('numberClick' => 'desc') );;
        $jsonContent = $serializer->serialize($all_articles, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);
        $articles = new Response($jsonContent);
        $articles->headers->set('Content-Type', 'application-json');
        $articles->headers->set('Access-Control-Allow-Origin', '*');
        $articles->headers->set('Access-Control-Allow-Methods', 'GET, OPTIONS');
        return $articles;
    }

    /**
     * @Route("/new", name="article_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserRepository $userRepository, CategorieRepository $categorieRepository): Response
    {
      $token = $request->headers->get('Authorization');
      if (in_array("ROLE_ADMIN", $userRepository->isAuth($token)->role)) {
        $article = new Article();
        $article->setTitle($request->request->get('title'));
        $article->setDescription($request->request->get('description'));
        $article->setPrice($request->request->get('price'));
        $article->setCreatedAt(new \DateTime());
        $article->setSpecification(json_decode($request->request->get('specifications')));
        $article->setStock($request->request->get('stock'));

        $cat = $categorieRepository->findCategory($request->request->get('categorie'));
        $em = $this->getDoctrine()->getManager();
        if($cat != []) {
          $categorie = $this->getDoctrine()->getRepository(Categorie::class)->find($cat[0]['id']);
        } else {
          $categorie = new Categorie();
          $categorie->setCategorie($request->request->get('categorie'));
          $article->setCategorie($categorie);
          $em->persist($categorie);
        }
        $article->setCategorie($categorie);

        $pictureFile = [];
        $i = 0;
        while ($request->files->get('pic' . $i) != '') {
          $pictureFile[] = $request->files->get('pic' . $i);
          $i++;
        }
        for ($j = 0; $j < count($pictureFile); $j++) {
          $originalFilename = pathinfo($pictureFile[$j]->getClientOriginalName(), PATHINFO_FILENAME);
          $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
          $newFilename = $safeFilename . '-' . uniqid() . '.' . $pictureFile[$j]->guessExtension();
          try {
            $pictureFile[$j]->move('image/article', $newFilename);
          } catch (FileExeption $e) {
            $msg = ['success' => $e];
          }
          $picture = new Picture();
          $picture->setArticleId($article);
          $picture->setPath($newFilename);
          $em->persist($picture);
        }
        $em->persist($article);

        if ($em->flush() == null) $msg = ['success' => 'Article Saved'];
        else $msg = ['success' => 'An error occured'];
      } else {
        $msg = ['error' => 'An error occured'];
      }

      $ret = new Response(json_encode($msg));
      $ret->headers->set('Content-Type', 'application/json');
      $ret->headers->set('Access-Control-Allow-Origin', '*');
      $ret->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS');
      return $ret;
    }

    /**
     * Find Article by Id (Article's details)
     * @Route("/{id}", name="article_show", methods={"GET"})
     */
    public function show($id, Article $article, ArticleRepository $articleRepository): Response
    {

        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $article = $articleRepository->find($id);
        $jsonContent = $serializer->serialize($article, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);
        $article = new Response($jsonContent);
        $article->headers->set('Content-Type', 'application-json');
        $article->headers->set('Access-Control-Allow-Origin', '*');
        $article->headers->set('Access-Control-Allow-Methods', 'GET, OPTIONS');
        return $article;
    }

    /**
     * @Route("/picture/{id}", name="article_picture", methods={"GET"})
     */
    public function getPicture($id, Article $article, PictureRepository $pictureRepository): Response
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $all_picture = $pictureRepository->findByArticleId($id);
        $jsonContent = $serializer->serialize($all_picture, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        $picture = new Response($jsonContent);
        $picture->headers->set('Content-Type', 'application-json');
        $picture->headers->set('Access-Control-Allow-Origin', '*');
        $picture->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS');
        return $picture;
    }

    /**
     * @Route("/{id}/edit", name="article_edit", methods={"GET", "POST"})
     */
    public function edit($id, Request $request, ArticleRepository $articleRepository, Article $article, UserRepository $userRepository): Response
    {        
        $token = $request->headers->get('Authorization');
        
        if (in_array("ROLE_ADMIN", $userRepository->isAuth($token)->role)) {
        
            $article->setTitle($request->request->get('title'));
            $article->setDescription($request->request->get('description'));
            $article->setPrice($request->request->get('price'));
            $article->setSolde($request->request->get('solde'));
            $article->setStock($request->request->get('stock'));
            $article->setCreatedAt(new \DateTime());
            $article->setSpecification(json_decode($request->request->get('specifications')));                        
            
            $em = $this->getDoctrine()->getManager();

            $pictureFile = [];
            $i = 0;
            while ($request->files->get('pic' . $i) != '') {
                $pictureFile[] = $request->files->get('pic' . $i);
                $i++;
            }
            for ($j = 0; $j < count($pictureFile); $j++) {
                $originalFilename = pathinfo($pictureFile[$j]->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $pictureFile[$j]->guessExtension();
                try {
                    $pictureFile[$j]->move('image/article', $newFilename);
                } catch (FileExeption $e) {
                    $msg = ['success' => $e];
                }
                $picture = new Picture();
                $picture->setArticleId($article);
                $picture->setPath($newFilename);
                $em->persist($picture);
            }
        
            $em->persist($article);

            if ($em->flush() == null) $msg = ['success' => 'Article updated successfuly'];
            else $msg = ['success' => 'Sorry, some error occured'];
        
        }else{
            $msg = ['error' => 'Action Denied! Bad Credentials :('];
        }
        $ret = new Response(json_encode($msg));
        $ret->headers->set('Content-Type', 'application/json');
        $ret->headers->set('Access-Control-Allow-Origin', '*');
        $ret->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS');
        return $ret;        
    }

    /**
     * Delete Article by Id
     * @Route("/{id}", name="article_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Article $article, PictureRepository $pictureRepository, UserRepository $userRepository): Response
    {
      $token = $request->headers->get('Authorization');
      if (in_array("ROLE_ADMIN", $userRepository->isAuth($token)->role)) {
        $all_picture = $pictureRepository->findByArticleId($article->getId());
        $em = $this->getDoctrine()->getManager();
        if ($all_picture != null || count($all_picture) > 0) {
          for ($i=0; $i < count($all_picture); $i++) {
            $em->remove($all_picture[$i]);
          }
        }
        $em->remove($article);
        $em->flush();
        $msg = 'Article deleted successfully !';
      } else {
        $msg = 'You can\'t delete this article';
      }
        $article = new Response($msg);
        $article->headers->set('Content-Type', 'application-json');
        $article->headers->set('Access-Control-Allow-Origin', '*');
        $article->headers->set('Access-Control-Allow-Methods', 'DELETE');
        return $article;
    }

    /**
     * @Route("/search/{param}", name="search", methods={"GET"})
     */
    public function search($param, Request $request, ArticleRepository $articleRepository): Response
    {
        $articles = $articleRepository->searchArticle($param);
        $article = new Response(json_encode($articles));
        $article->headers->set('Content-Type', 'application-json');
        $article->headers->set('Access-Control-Allow-Origin', '*');
        $article->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS');
        return $article;
    }

    /**
     * @Route("/{id}/click/{nbclick}", name="click_article", methods={"GET"})
     */
     public function view($id, $nbclick, Request $request, Article $article, ArticleRepository $articleRepository) {
       $nb_click = $article->getNumberClick();
       if ($nb_click == null) {
         $nb_click = $nbclick;
       } else {
         $nb_click += $nbclick;
       }
       $article->setNumberClick($nb_click);
       $em = $this->getDoctrine()->getManager();
       $em->persist($article);
       $em->flush();

       $rep = new Response('done');
       $rep->headers->set('Content-Type', 'application-json');
       $rep->headers->set('Access-Control-Allow-Origin', '*');
       $rep->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS');
       return $rep;
     }


}
