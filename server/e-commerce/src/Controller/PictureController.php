<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Picture;
use App\Form\ArticleType;
use App\Repository\PictureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/picture")
 */
class PictureController extends AbstractController
{
    /**
     * @Route("/get/{articleId}", name="picture_get", methods={"GET"})
     */
    public function show($articleId, Picture $picture, PictureRepository $pictureRepository): Response
    {
        // dd($articleId);
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $all_picture = $pictureRepository->findByArticleId($id);
        $jsonContent = $serializer->serialize($all_picture, 'json');

        $picture = new Response($jsonContent);
        $picture->headers->set('Content-Type', 'application-json');
        $picture->headers->set('Access-Control-Allow-Origin', '*');
        $picture->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS');
        return $picture;
    }

    /**
     * @Route("/find/{name}", name="picture_name", methods={"GET"})
     */
    public function findPicture($name): Response
    {
        $file = fopen('image/article/' . $name, 'rb');
        $str = base64_encode(stream_get_contents($file));
        // return $file;
        $picture = new Response($str);
        $picture->headers->set('Content-Type', 'image/jpg');
        $picture->headers->set('Access-Control-Allow-Origin', '*');
        $picture->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS');
        return $picture;
    }
}
