<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Repository\AdresseRepository;
use App\Repository\CreditCardRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\Adresse;
use App\Entity\CreditCard;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="user_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="user_new", methods={"GET","POST"})
     */
    public function new(Request $request,UserPasswordEncoderInterface $encoder): Response
    {

        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $plainPassword = $user->getPassword();
            $encoded = $encoder->encodePassword($user, $plainPassword);

            $user->setRoles(['ROLE_USER']);
            $user->setIsOnline(false);
            $user->setPassword($encoded);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_index');
        }
        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        $ret = new Response(json_encode(
            [
                "email" => $user->getEmail(),
                "firstname" => $user->getFirstname(),
                "lastname" => $user->getLastname(),
                "phone" => $user->getPhone()
            ]
        ));
        $ret->headers->set('Content-Type', 'application-json');
        $ret->headers->set('Access-Control-Allow-Origin', '*');
        $ret->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS');

        return $ret;
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user, UserPasswordEncoderInterface $encoder): Response
    {

        $user_data = json_decode($request->getContent(), true);

        $user->setFirstname($user_data['firstname']);
        $user->setLastname($user_data['lastname']);
        $user->setEmail($user_data['email']);
        $user->setPhone($user_data['phone']);

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);

        $em->flush();

        $msg = ['success' => 'Article Saved'];
        $ret = new Response(json_encode($msg));
        $ret->headers->set('Content-Type', 'application/json');
        $ret->headers->set('Access-Control-Allow-Origin', '*');
        $ret->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS');
        return $ret;
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        $msg = ['success' => 'User deleted'];
        $ret = new Response(json_encode($msg));
        $ret->headers->set('Content-Type', 'application/json');
        $ret->headers->set('Access-Control-Allow-Origin', '*');
        $ret->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS');
        return $ret;
    }

    /**
     * @Route("/{id}/adress", name="user_adress", methods={"POST"})
     */
    public function userAdress($id, Request $request, UserRepository $userRepository): Response
    {
        $adresse = new Adresse;
        $user = $userRepository->find($id);

        $adresse->setUserId($user);
        $adresse->setAdresse($request->request->get('adress'));
        $adresse->setCity($request->request->get('city'));
        $adresse->setPostCode($request->request->get('postcode'));
        $adresse->setContry($request->request->get('country'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($adresse);

        $em->flush();

        $msg = ['success' => 'Adresse Saved'];
        $ret = new Response(json_encode($msg));
        // $ret = new Response(json_encode($request->request->get('city')));
        $ret->headers->set('Content-Type', 'application/json');
        $ret->headers->set('Access-Control-Allow-Origin', '*');
        $ret->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS');
        return $ret;
    }

    /**
     * @Route("/{id}/card", name="user_card", methods={"POST"})
     */
    public function userCard($id, Request $request, UserRepository $userRepository): Response
    {
        $card = new CreditCard;
        $user = $userRepository->find($id);

        $date = new \DateTime($request->request->get('expiration'));

        $card->setUserId($user);
        $card->setCardNumber($request->request->get('card'));
        $card->setExpirationDate($date);


        $em = $this->getDoctrine()->getManager();
        $em->persist($card);
        $em->flush();

        $msg = ['success' => 'Card Saved'];
        $ret = new Response(json_encode($msg));

        $ret->headers->set('Content-Type', 'application/json');
        $ret->headers->set('Access-Control-Allow-Origin', '*');
        $ret->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS');
        return $ret;
    }

    /**
     * @Route("/{id}/getAdresse", name="user_getAdress", methods={"GET"})
     */
    public function getAdress($id, AdresseRepository $adresseRepository): Response
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $adresse = $adresseRepository->findByUserId($id);

        $jsonContent = $serializer->serialize($adresse, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        // dd($adresse);

        $ret = new Response($jsonContent);
        $ret->headers->set('Content-Type', 'application/json');
        $ret->headers->set('Access-Control-Allow-Origin', '*');
        $ret->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS');
        return $ret;
    }

    /**
     * @Route("/delete/adresse/{id}", name="user_deleteAdresse", methods={"DELETE"})
     */
    public function deleteAdresse(Request $request, AdresseRepository $adresseRepository, UserRepository $userRepository, Adresse $adresse): Response
    {
        $token = $request->headers->get('Authorization');
        if (in_array("ROLE_USER", $userRepository->isAuth($token)->role)){

            $adresse = $adresseRepository->find($adresse->getId());
            $em = $this->getDoctrine()->getManager();

            $em->remove($adresse);
            $em->flush();

            $adresse = new Response('Adresse deleted successfully !');
            $adresse->headers->set('Content-Type', 'application-json');
            $adresse->headers->set('Access-Control-Allow-Origin', '*');
            $adresse->headers->set('Access-Control-Allow-Methods', 'DELETE');
            return $adresse;
        }
    }

    /**
     * @Route("/{id}/getCB", name="user_getCB", methods={"GET"})
     */
    public function getCb($id, CreditCardRepository $creditCardRepository): Response
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $credit = $creditCardRepository->findByUserId($id);

        $jsonContent = $serializer->serialize($credit, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        $ret = new Response($jsonContent);
        $ret->headers->set('Content-Type', 'application/json');
        $ret->headers->set('Access-Control-Allow-Origin', '*');
        $ret->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS');
        return $ret;
    }

    /**
     * @Route("/delete/CB/{id}", name="user_deleteCB", methods={"DELETE"})
     */
    public function deleteCB(Request $request, CreditCard $creditCard, CreditCardRepository $creditCardRepository, UserRepository $userRepository): Response
    {
        $token = $request->headers->get('Authorization');
        if (in_array("ROLE_USER", $userRepository->isAuth($token)->role)){
            $cb = $creditCardRepository->find($creditCard->getId());
            $em = $this->getDoctrine()->getManager();

            $em->remove($cb);
            $em->flush();

            $cb = new Response('CreditCard deleted successfully !');
            $cb->headers->set('Content-Type', 'application-json');
            $cb->headers->set('Access-Control-Allow-Origin', '*');
            $cb->headers->set('Access-Control-Allow-Methods', 'DELETE');
            return $cb;
        }
    }

}
