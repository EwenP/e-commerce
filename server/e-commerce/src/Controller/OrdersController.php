<?php

namespace App\Controller;

use App\Entity\Orders;
use App\Entity\User;
use App\Form\ArticleType;
use App\Repository\OrdersRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * @Route("/order")
 */
class OrdersController extends AbstractController
{
     /**
     * @Route("/new", name="orders_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $orders = new Orders();
   
            $user = $this->getDoctrine()->getRepository(User::class)->find($request->request->get('id'));
            $orders->setArticles($request->request->get('article'));
            $orders->setUserId($user);
            $orders->setCreatedAt( new \DateTime());
            // $orders->setStatus('null');
            $orders->setAdress($request->request->get('adresse'));
            $orders->setCardNumber($request->request->get('cb'));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($orders);
            $entityManager->flush();

            $msg = ['msg' => $request->request->get('id')];
            $response = new JsonResponse(json_encode($msg));
            $response->headers->set('Content-Type', 'application-json');
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $response->headers->set('Access-Control-Allow-Methods', 'POST, PUT');
            return $response;
        // }
    }

}
