<?php

namespace App\Controller;

use App\Entity\Categories;
use App\Repository\CategorieRepository;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CategorieController extends AbstractController
{
    /**
     * @Route("/categorie", name="categorie")
     */
    public function index(CategorieRepository $repo): Response
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $all_categories = $repo->findAll();
        $jsonContent = $serializer->serialize($all_categories, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);
        $categories = new Response($jsonContent);
        $categories->headers->set('Content-Type', 'application-json');
        $categories->headers->set('Access-Control-Allow-Origin', '*');
        $categories->headers->set('Access-Control-Allow-Methods', 'GET, OPTIONS');
        return $categories;
    }
}
