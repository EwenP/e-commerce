/************************************/
/*    Code to get all info about    */
/*  one article from the Data Base  */
/************************************/

import React, { Component } from 'react';
import axios from 'axios';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { IoIosStar, IoIosStarOutline } from "react-icons/io";
import { MdEuroSymbol } from "react-icons/md";
import Specification from './Specification';

class ShowArticle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      articles:{},
      picData: [],
      picAlt: [],
      error:""
    }
  }

  componentDidMount(){
    this.showArticleDetailsHandler()
    this.showView()
  };

  showView() {
    let url = window.location.href.substring(window.location.href.lastIndexOf('/'));
    let id = url.replace('/','');
    axios.get(`http://127.0.0.1:8000/article/${id}/click/1`)
    .then(res => {return true}).catch(err => {return false});
  }

  showArticleDetailsHandler(){
    let url = window.location.href.substring(window.location.href.lastIndexOf('/'));
    let id = url.replace('/','');
    axios.get(`http://127.0.0.1:8000/article/${id}`)
    .then(res => {
      var article = res.data;
      axios.get(`http://127.0.0.1:8000/article/picture/${article.id}`).then(res => {
        var picPath = [];
        var picAlt = [];
        if (res.data === null || res.data.length === 0) {
          picPath[0] = "noPic.jpg"
          picAlt[0] = "no picture avalable";
        } else {
          for (let i = 0; i < res.data.length; i++) {
            picPath[i] = res.data[i].path;
            picAlt[i] = res.data[i].alt;
          }
        }
        this.setState({articles: article, picAlt: picAlt});
        for (let i = 0; i < picPath.length; i++) {
          axios.get(`http://127.0.0.1:8000/picture/find/${picPath[i]}`).then(res => {
            let statePicData = this.state.picData;
            statePicData.push(res.data);
            this.setState({picData: statePicData})
            return true;
          }).catch(err => {return false;})
        }
      })
      .catch( err => {return false})
    })
    .catch( err => {
      this.setState({error: "Article not find."})
      return false;
    });
  }
  render(){
    if (this.state.error !== "") return (<div> {this.state.error} </div>)
    return(
      <Container fluid={true}>
          <Row className="vh-50 justify-content-center">
              <Col xl={4} lg={6} md={6} sm={6} className="">
                  <div className="mt-5 mr-5 mx-auto align-middle">
                  {
                    this.state.picData.map((pic, index) => {
                      let src = `data:image/jpeg;base64,${pic}`
                      return (<img key={index} src={src} alt={this.state.picAlt[0]} id="img_articles" />)
                    })
                  }
                  </div>
              </Col>
              <Col xl={4} lg={6} md={6} sm={6} className="font-montserra">
                  <div className="mt-5">
                      <h1 className="text-secondary">{this.state.articles.title}</h1>
                      <hr></hr>
                  </div>
                  <div className="mt-3">
                      <h1 className="d-inline align-middle" style={{'color':'red'}}>{this.state.articles.price}</h1>
                      <span className="align-middle"><MdEuroSymbol size={50} color="red"/></span>
                  </div>
                  <div className="m-0 p-0">
                      <small className="m-0 p-0 font-weight-bold">TTC</small>
                      <hr></hr>
                  </div>
                  <div>
                      <Button onClick={() => {this.props.addProduct(this.state.articles.id)}} variant="danger" size="lg">Add to Basket</Button>
                      <hr></hr>
                  </div>
                  <div className="mt-1 m-0">
                      <p >Stock : <span className="badge badge-success">{this.state.articles.stock}</span></p>
                      <hr></hr>
                  </div>

              </Col>
          </Row>
          <Row className="vh-50 justify-content-center p-5">
              <Col xl={4} lg={4} md={4} sm={3} className="border-right">
                  <div className="mt-5 text-justify">
                      <p className="text-secondary font-weight-bold">Description</p>
                  </div>
                  <div >
                      <p className="text-secondary m-0 p-0">{this.state.articles.description}</p>
                  </div>
              </Col>
              <Col xl={4} lg={4} md={4} sm={3}>
                <div className="mt-5 text-center">
                  <p className="text-secondary font-weight-bold">Spécifications</p>
                </div>
                <div>
                  <Specification spec={this.state.articles.specification}/>
                </div>
              </Col>
          </Row>
      </Container>
    )
  }
}

export default ShowArticle;
