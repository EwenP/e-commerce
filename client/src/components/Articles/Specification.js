/************************************/
/*  Code to display specifisations  */
/************************************/

import React, { Component } from 'react';

class Specification extends Component {

  render() {
    var specs = this.props.spec;
    if (specs !== undefined && specs !== null) {
      return(
        <div>
          <div id="spec">
            <ul className=" text-secondary">
              {specs.map((spec, key) => {
                return (
                  <li key={key}>{spec.specificationTitle} : {spec.specificationContent}</li>
                );
              })}
            </ul>
          </div>
        </div>
        )
    } else {
      return (<div>Specification not found :/</div>)
    }
  }
}

export default Specification;
