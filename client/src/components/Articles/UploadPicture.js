/************************************/
/*     Code to upload many file     */
/************************************/

import React, { Component } from 'react';

class UploadPicture extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addPicture: true
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.function(e.target.picture.files);
  }

  displayPictureInput() {
    return (
      <form onSubmit={(e) => {this.handleSubmit(e)}} encType="multipart/form-data">
        <input type="file" name="picture" accept="image/png, image/jpeg" multiple />
        <input type="submit" name="upload" value="Upload" className="btn btn-outline-primary mt-3 mb-3"/>
      </form>
    );
  }

  render() {
    return (
      <div>
        {this.displayPictureInput()}
      </div>
    );
  }
}
export default UploadPicture;
