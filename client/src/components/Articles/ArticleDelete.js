/************************************/
/*    Code to delete an article     */
/*        from the Data Base        */
/************************************/

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router';
import { Container, Row, Col } from 'react-bootstrap'
import axios from 'axios';

class ArticleDelete extends Component{

  constructor(props) {
    super(props);
    this.state = {
      auth: props.role.indexOf("ROLE_ADMIN") === -1 ? <Redirect to='/' /> : <></>,
      msg:'',
      redirect: false
    }
  }

  deleteArticleHandler = () => {
    let url = window.location.href.substring(window.location.href.lastIndexOf('/'));
    let id = url.replace('/','');
    axios({
        url: `http://127.0.0.1:8000/article/${id}`,
        method: 'DELETE',
        headers: {
          'Authorization': localStorage.getItem('token')
        }
      }).then(res => {
          this.setState({ msg: res.data });
      }).catch(err => {
        this.setState({msg: "Article does not exist."})
      })
  }

  render(){
    let msg;
    if (this.state.msg !== ''){
      msg = <h1 className="text-white font-montserra">{this.state.msg}</h1>
    } else {
      msg =
        <>
          <h1 className="font-montserra">Are you sure to delete this article?</h1>
          <button onClick={this.deleteArticleHandler} className="btn btn-outline-danger">Confirm</button>
        </>
    }
    return (
      <Container>
        {this.state.auth}
        <Row className="justify-content-center align-items-center">
          <Col>
            <div className="jumbotron text-center text-justify gradient">
              {msg}
              <Link to="/" className="btn btn-outline-secondary ml-3">Back</Link>
            </div>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default ArticleDelete;
