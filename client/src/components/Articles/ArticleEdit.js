import React, { Component } from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import axios from 'axios';

class ArticleEdit extends Component {
    
    state = { 
        articleId: '',
        title: '',
        description: '',
        price: '',
        solde: '',
        stock: '',
        imagePreview: '',
        categoryId: '',
        specificationTitle: '',
        specificationContent: '',
        specifications: [],
        picture: [],
        picData: [],
        data: {},        
        picAlt: [],
        apiResponseSuc: '',
        apiResponseErr: ''
     }

    componentDidMount(){
        this.getArticle();
    }
    
    getArticle = () => {
        let url = window.location.href.substring(window.location.href.lastIndexOf('/'));
        let id = url.replace('/','');        
        axios.get(`http://127.0.0.1:8000/article/${id}`)
            .then(res => {
                var article = res.data;
                this.setState({ specifications: res.data.specification, solde: res.data.solde })                
                axios.get(`http://127.0.0.1:8000/article/picture/${article.id}`)
                    .then(res => {                        
                        var picPath = [];
                        var picAlt = [];
                        if (res.data === null || res.data.length === 0) {
                            picPath[0] = "noPic.jpg"
                            picAlt[0] = "no picture avalable";
                        } else {
                            for (let i = 0; i < res.data.length; i++) {
                            picPath[i] = res.data[i].path;
                            picAlt[i] = res.data[i].alt;
                            }
                        }                        
                        this.setState({                             
                            articleId: article.id,
                            title: article.title, 
                            description: article.description,
                            price: article.price,
                            solde: article.solde,
                            stock: article.stock,                            
                            categoryId: article.categorie.categorie, 
                            picAlt: picAlt                            
                        })
                        
                        for (let i = 0; i < picPath.length; i++) {
                            axios.get(`http://127.0.0.1:8000/picture/find/${picPath[i]}`)
                                .then(res => {
                                    let statePicData = this.state.picData;
                                    statePicData.push(res.data);
                                    this.setState({ picData: statePicData })
                                })
                                .catch( err => {console.log(err) })
                        }                        
                    })
                    .catch( err => console.log('[PICTURE-ERR]', err))      
            })
            .catch( err => { 
                this.setState({ error: "Article not find." }) 
            });
    }
    
    sendData = (data) => {        
        let token = localStorage.getItem('token');
        let id = this.state.articleId;
            axios.post(`http://127.0.0.1:8000/article/${id}/edit`, data, {
            headers: {
                'accept' : "application/json",
                'Content-Type': 'multipart/form-data',
                'Authorization': token
            }
        }).then(res => {
            console.log(res.data);
            if(res.data.success){
                this.setState({ apiResponseSuc: res.data.success })
                // console.log(res.data.success)
            }else {
                this.setState({ apiResponseErr: res.data.error })
            }
        })
        .catch( err => {
            console.log(err);
        })
      }
    
    handleSubmitMain = (e) => {
        e.preventDefault();
        let data = {
            title: this.state.title,
            description: this.state.description,
            price: this.state.price,                        
            stock: this.state.stock,
            solde: this.state.solde,
            specifications: this.state.specifications
        };        
        let formData = new FormData();
        
        let picture = this.state.picture;
        for (var [key, value] of Object.entries(data)) {
          if (key === 'specifications') value = JSON.stringify(value);
          formData.append(key, value);
        }

        if (picture !== null && picture !== undefined) {
          for (const [key, value] of Object.entries(picture)) {
            let newKey = 'pic' + key;
            formData.append(newKey, value);
          }
        }        
        this.sendData(formData);
    }

    handleInputChange =  (e) => {        
        var key = e.target.name
        this.setState({ [key]: e.target.value })        
    }

    addSpecificationHandler =  (e) => {        
        let specifications = this.state.specifications;
        let newSpec = { 
            specificationTitle: this.state.specificationTitle, 
            specificationContent: this.state.specificationContent
        };
        if (specifications === null)
            specifications = [newSpec];
        else
            specifications.push(newSpec);
        this.setState({ specifications: specifications });
    }
        
    onChangeInputFileHandler = (e) => {
        e.preventDefault();        
        let file = e.target.files;        
        this.setState({ picture: file, imagePreview: window.URL.createObjectURL(e.target.files[0]) });
    }
    
    handleFormSubmit = (e) => {
        e.preventDefault();        
    }

    displaySpecification = () => {
        let spec = this.state.specifications;
        if (spec !== null) {
                let ret = 
                spec.map( (s, index) => {
                    return (
                        <div key={index} className="">
                            <p className="m-0 p-0 text-secondary">{ s.specificationTitle} : {s.specificationContent }</p>                            
                        </div>
                    );
                })
            return ret;
        }
    }
    
    render() { 
        let span;   
        if (this.state.apiResponseSuc){
            span = <span className="alert alert-success text-center">{this.state.apiResponseSuc} </span>
        }else if(this.state.apiResponseErr){
            span = <span className="alert alert-danger text-center">{this.state.apiResponseErr}</span>
        } 
        else{
            span = null;
        };  
        return ( 
            <Container fluid={true} className="m-0 p-0">
                <Row className="justify-content-center mt-5 ">
                    <div className="text-center">
                        <h1 className="text-secondary font-weight-bold">EDIT ARTICLE</h1>
                        {this.state.apiResponseSuc ? <Link to="/">Back to Home Page</Link> : null}
                    </div>
                                       
                </Row>                  
                <Row className="">
                    <Col className="p-5 m-5">
                        {span}
                        <Form onSubmit={this.handleSubmitMain} className="text-primary font-weight-bold">
                            <Form.Row>
                                <Form.Group as={Col}>
                                    <Form.Label>Titre*</Form.Label>
                                    <Form.Control onChange={this.handleInputChange} name="title" type="text" value={this.state.title || ''}/>                                                                
                                </Form.Group>
                                <Form.Group as={Col}>
                                    <Form.Label>Category*</Form.Label>
                                    <Form.Control onChange={this.handleInputChange} name="categoryId" type="" value={this.state.categoryId || ''}/>                                                                
                                </Form.Group>
                                
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col}>
                                    <Form.Label>Price*</Form.Label>
                                    <Form.Control onChange={this.handleInputChange} name="price" type="number" value={this.state.price || ''} />                                                                
                                </Form.Group>
                                <Form.Group as={Col}>
                                    <Form.Label>Stock*</Form.Label>
                                    <Form.Control onChange={this.handleInputChange} name="stock" type="number" value={this.state.stock || ''} />                                                                
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col}>
                                    <Form.Label>Solde</Form.Label>
                                    <Form.Control onChange={this.handleInputChange} name="solde" type="number" value={this.state.solde || ''} />                                                                
                                </Form.Group>
                            </Form.Row>
                                <Form.Group>
                                    <Form.Label>Description*</Form.Label>
                                    <Form.Control onChange={this.handleInputChange} name="description" as="textarea" value={this.state.description || ''} style={{ height:'5rem' }}/>                                                                
                                </Form.Group>
                            
                            <div className="mt-2 mb-5">
                                <Form.Row>
                                    <Form.Group as={Col}>
                                        <Form.Label>Specifications Title</Form.Label>
                                        <Form.Control onChange={this.handleInputChange} name="specificationTitle" type="text" value={this.state.specificationTitle || ''} />
                                    </Form.Group>
                                    
                                    <Form.Group as={Col}>
                                        <Form.Label>Specifications Content</Form.Label>
                                        <Form.Control onChange={this.handleInputChange} name="specificationContent" type="text" value={this.state.specificationContent || ''} />
                                    </Form.Group>
                                    <Button onClick={this.addSpecificationHandler} variant="outline-primary">Add</Button>                                    
                                </Form.Row>
                            </div>
                                
                                <Button variant="primary" type="submit" block>SAVE ALL</Button>
                        </Form>
                    </Col>
                    <Col className="p-5 m-5 border">
                        <div className="d-flex">
                        {
                          this.state.picData.map((pic, index) => {
                            let src = `data:image/jpeg;base64,${pic}`
                            return (
                                <div key={index}>
                                    <img  src={src} alt={this.state.picAlt[0]} width="50%" />
                                    <Form.Check type="checkbox" />
                                </div>
                                )
                          })
                        }
                        </div><hr></hr>
                        <div className="mb-3">
                            <Form encType="multipart/form-data">                                
                                <div className="upload-btn-wrapper">
                                    <input onChange={this.onChangeInputFileHandler} type="file" className="" name="picture" ref='fileUpload' accept="image/png, image/jpeg" multiple/>
                                    <Button  onClick={this.uploadImagesHandler} variant="outline-primary">Upload Image here</Button>                                                       
                                </div>
                            </Form>
                            <div>
                                <img src={this.state.imagePreview} alt={this.state.picture} width="25%" />
                            </div>
                        </div><hr></hr>
                        <Row>                        
                            <Col>
                                <div><p>SPECIFICATIONS</p></div>
                                <div>
                                    {this.displaySpecification()}
                                </div>
                            </Col>
                            <Col>
                            </Col>
                        </Row>
                    </Col>
                </Row>        
            </Container>
        );
    }
}
export default ArticleEdit;


