/************************************/
/*  Code to get and display info    */
/*      of vignette article         */
/************************************/

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap';
import axios from 'axios';
import ArticlesFilter from './ArticlesFilter';
import { MdDeleteSweep } from "react-icons/md";
import { FaRegEdit } from "react-icons/fa";


class VignetteArticle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      articles:[],
      isFiltered: false
    }
  }
    componentDidMount() {
        this.getArticles('');
    }

    getArticles(cat) {
        this.setState({ articles: [] })
        axios.get(`http://127.0.0.1:8000/article/${cat}`).then(res => {
            res.data.map((article) => {
              axios.get(`http://127.0.0.1:8000/article/picture/${article.id}`).then(res => {
                var picPath = "noPic.jpg"
                var picAlt = "no picture avalable";
                if (res.data !== null && res.data.length !== 0) {
                 var picPath = res.data[0].path;
                 var picAlt = res.data[0].alt;
                }
                axios.get(`http://127.0.0.1:8000/picture/find/${picPath}`).then(res => {
                  article.picData = res.data;
                  article.picAlt = picAlt;
                  articles.push(article);
                  this.setState({articles: articles})
                }).catch(err => {return false;})
                var articles = this.state.articles;
              }).catch( err => { return false;})
              return true;
            })
        }).catch( err => {return false;});
    }

    inputChangeHandler = (e) => {
        let value = e.target.value;
        if (value === "")
          this.getArticles(value);
        else
          this.getArticles('categorie/' + value);
    }

    displayDelete = (id) => {
      if (this.props.role[0] === "ROLE_ADMIN") {
        return (
          <Link to={`/article/delete/${id}`} className="btn btn-outline-danger m-3">
              <MdDeleteSweep size={30} data-toggle="tooltip" title="Delete Article"/>
          </Link>
        );
      }
    }

    displayEdit = (id) => {
        if (this.props.role[0] === "ROLE_ADMIN") {
          return (
            <Link to={`/article/edit/${id}`} className="btn btn-outline-warning">
                <FaRegEdit size={30} data-toggle="tooltip" title="Edit Article" />
            </Link>
          );
        }
      }

    render() {
        return(
            <Container fluid={true}>
                <Row className="">
                    <Col xl={2} lg={2} md={2}  className="border-right">
                        <ArticlesFilter handleSubmit={this.handleSubmit} inputChangeHandler={this.inputChangeHandler} />
                    </Col>
                    <Col xl={10} lg={10} md={10} sm={10} className="">
                        <div className="d-flex flex-wrap ml">
                            {this.state.articles.map(article => {
                                if (article !== null) {
                                var src =`data:image/jpeg;base64,${article.picData}`
                                return(
                                  <div className="card m-3 p-3 font-montserra" key={article.id}>
                                    <Link className="text-secondary" to={`/article/${article.id}`}>
                                      <img src={src} style={{width: "100%"}} alt={article.picAlt}></img>
                                      <h3>{article.title}</h3>
                                    </Link>
                                    <p className="price">{article.price}€</p>
                                    <p>{article.description}</p>
                                    <p><button 
                                        onClick={() => {this.props.addProduct(article.id)}} >Add to Basket</button></p>
                                    <div className="d-inline">
                                        {this.displayDelete(article.id)}
                                        {this.displayEdit(article.id)}
                                    </div>
                                  </div>
                                );
                                }
                                return (<div>No article find</div>)
                            })}
                        </div>
                    </Col>
                </Row>
             </Container>
        );

    }
}

export default VignetteArticle;
