/************************************/
/*    Code to get all categorys     */
/*       from the Data Base         */
/*       to put it in select        */
/************************************/

import React, { Component } from 'react';
import axios from 'axios';

class ArticlesFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: []
    }
  }

  componentDidMount(){
    this.findAllCategories();
  }

  findAllCategories = () => {
    axios.get('http://127.0.0.1:8000/categorie')
    .then( res => {
        this.setState({ categories: res.data })
        return true;
    })
    .catch( err =>{
      console.log(err);
      return false;
    })
  }
  render() {
    return (
      <div className="p-5">
        <fieldset>
          <div className="form-group">
            <label htmlFor="exampleSelect1" className="font-montserra">Filter by Category</label>
            <select onChange={this.props.inputChangeHandler}  className="form-control" id="exampleSelect1">
              <option className="font-montserra" value="">All</option>
              {this.state.categories.map(categorie => {
                return (
                  <option className="font-montserra" key={categorie.id} value={categorie.id}>{categorie.categorie}</option>
                )
              })}
            </select>
          </div>
        </fieldset>
      </div>
     );
  }
}

export default ArticlesFilter;
