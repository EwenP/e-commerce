/************************************/
/*    Code to add an article        */
/*       in the Data Base           */
/************************************/

import React, { Component } from 'react';
import { Redirect } from 'react-router';
import axios from 'axios';
import UploadPicture from './UploadPicture';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import "./Article.css";

class ArticleNewBetter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      auth: props.role.indexOf("ROLE_ADMIN") === -1 ? <Redirect to='/' /> : <></>,
      displayMainForm: true,
      correctMainFormData: false,
      title: '',
      description: '',
      price: '',
      categorie: '',
      stock: '',
      specificationTitle: '',
      specificationContent: '',
      imagePreview: '',
      specifications: [],
      picture: [],
      data: {},
      redirect: <> </>
    }
  }

  handleBlurMainForm(e) {
    if (e.target.value === "") {
      e.target.style.borderColor = "red";
      this.setState({correctMainFormData: false});
    } else {
      e.target.style.borderColor = "";
      this.setState({correctMainFormData: true});
    }
    var key = e.target.name
    this.setState({[key]: e.target.value});
  }

  handleBlurSpec(e) {
    let key = e.target.name
    this.setState({[key]: e.target.value});
  }

  handleAddSpec() {
    let spec = this.state.specifications;
    let newSpec = {specificationTitle: this.state.specificationTitle, specificationContent: this.state.specificationContent};
    spec.push(newSpec);
    this.setState({specifications: spec});
  }

  handleSubmitMain(e) {
    e.preventDefault();
    let data = {title: this.state.title,
                description: this.state.description,
                price: this.state.price,
                categorie: this.state.categorie,
                stock: this.state.stock,
                specifications: this.state.specifications};
    let formData = new FormData();
    let picture = this.state.picture;
    for (var [key, value] of Object.entries(data)) {
      if (key === 'specifications') value = JSON.stringify(value);
      formData.append(key, value);
    }
    if (picture !== null || picture !== undefined) {
      console.log(picture)
      for (const [key, value] of Object.entries(picture)) {
        let newKey = 'pic' + key;
        formData.append(newKey, value);
      }
    }
    this.sendData(formData);
  }

  saveFiles = (data) => {
    this.setState({ picture: data[0].name, imagePreview: window.URL.createObjectURL(data[0]) });
  }

  sendData(data) {
    axios.post('http://127.0.0.1:8000/article/new', data, {
      headers: {
        'accept' : "application/json",
        'Content-Type': 'multipart/form-data',
        'Authorization': localStorage.getItem('token')
      }
    }).then(res => {
      console.log(res.data);
      if (res.data.success === undefined) {
        document.getElementById('message').innerHTML = "Your are not authorized to do this";
        document.getElementById('message').className = "alert alert-danger";
      } else {
        document.getElementById('message').innerHTML = "Article saved !";
        document.getElementById('message').className = "alert alert-success";
        this.setState({redirect:<Redirect to='/' />});
      }
    })
    .catch( err => {
      document.getElementById('message').innerHTML = "Faild to save article";
      document.getElementById('message').className = "alert alert-danger";
    })
  }


  displayMainForm = () => {
    if (this.state.displayMainForm) {
      let spec = this.state.specifications;
      let form =
      <div>
            <h1 className="font-montserra"> Add Product </h1>
            <div className=" mx-auto ">
              <form action="#" className="form" method="POST" onSubmit={(e) => {this.handleSubmitMain(e);}}>
            <UploadPicture
              function={this.saveFiles}
              />
                <Form.Row>
                    <Form.Group as={Col}>
                        <label className="">Title*
                        <input onBlur={(e) => {this.handleBlurMainForm(e)}}
                        name="title" type="text"
                        placeholder="article title"
                        defaultValue={this.state.data.title}
                        className="form-control"/>
                        </label>
                    </Form.Group>
                    <Form.Group as={Col}>                        
                        <label className="">Price*
                        <input onBlur={(e) => {this.handleBlurMainForm(e)}}
                        name="price"
                        type="number"
                        placeholder="price"
                        defaultValue={this.state.data.price}
                        step="0.01" min="0"
                        className="form-control"/>
                        </label>                                  
                    </Form.Group>
                </Form.Row>
                        <label className="">Description*
                            <textarea onBlur={(e) => {this.handleBlurMainForm(e)}}
                            name="description"
                            rows="5"
                            className="form-control"
                            cols="55"
                            placeholder="Article description..."
                            defaultValue={(this.state.data.description === undefined) ? "" : this.state.data.description}/>
                        </label>
                <Form.Row>
                    <Form.Group as={Col}> 
                        <label className="">Catégory*
                        <input onBlur={(e) => {this.handleBlurMainForm(e)}}
                        name="categorie"
                        type="text"
                        placeholder="Catégorie"
                        defaultValue={this.state.data.categorie}
                        className="form-control"/>
                        </label>
                    </Form.Group>
                    <Form.Group>
                        <label className="">Stock*
                        <input onBlur={(e) => {this.handleBlurMainForm(e)}}
                        name="stock"
                        type="number"
                        placeholder="stock"
                        defaultValue={this.state.data.stock}
                        className="form-control"
                        min="0"/>
                        </label>
                    </Form.Group>
                  </Form.Row>
                    <div className="mx-auto">
                        {
                            spec.map((s) => {
                            return <p className="font-montserra text-center"><span>{s.specificationTitle} : </span>{s.specificationContent}</p>
                            })
                        }
                    </div>
                  <div className="d-flex flex-wrap justify-content-center">
                      <div className="mr-3">
                          <input onBlur={(e) => this.handleBlurSpec(e)} name="specificationTitle" type="text" placeholder="new specification title" />
                      </div>
                      <div className="mb-3">
                          <input onBlur={(e) => this.handleBlurSpec(e)} name="specificationContent" type="text" placeholder="new specification content" />
                      </div>
                      <button onClick={() => {this.handleAddSpec()}} name="add" type="button" className="btn btn-sm btn-outline-primary ml-3"> Add </button>
                  </div>
                    <div className="text-center mt-3">
                          <input className="btn btn-success btn-block" name="save" type="submit" value="Save"/>
                    </div>
              </form>
            </div>
      </div>
      return form;
    }
  }

  render() {
    return (
        <Container>
          {this.state.redirect}
            <Row></Row>
            <Row>
                <Col>
                    <div>
                        {this.state.auth}
                        {this.displayMainForm()}
                    </div>                
                </Col>
                <Col className="justify-content-center">
                    <div className="text-center">
                        <img src={this.state.imagePreview} width="100%"/>
                    </div>
                </Col>
            </Row>
        </Container>
    );
  }
}
export default ArticleNewBetter;
