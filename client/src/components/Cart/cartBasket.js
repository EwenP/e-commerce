import React, { Component } from 'react';
import axios from 'axios';
import "./cart.css";
import AdresseBasket from "./../User/adresseBasket";
import CbBasket from "./../User/cbBasket";
import { Redirect } from 'react-router';

class CartBasket extends Component {
    constructor(props){
      super(props);
      this.state = {
          result:"",
          price: 0,
          message: "",
          redirect: <></>
      }
    }

    componentDidMount(){
      this.getInfoArticle()
    }

    getInfoArticle(){
      this.setState({result: '', price:0});
      var key="id_product";
      var ls = localStorage.getItem(key);
      if(ls !== '' && ls !== null) {
        var ids = ls.split(",").sort();
        var lastId = 0;
        var qty = [];
        ids.map((id) => {
          if (lastId !== id ) {
            qty[id] = this.countOccuranceInArray(ids, id);
            lastId = id;
            axios.get(`http://127.0.0.1:8000/article/${lastId}`).then((res) => {
              var articleData = res.data;
              var picPath = "noPic.jpg";
              articleData.picAlt = "no Pic avalable"
              axios.get(`http://127.0.0.1:8000/article/picture/${articleData.id}`).then(res => {
                if (res.data !== undefined && res.data !== null && res.data.length > 0) {
                  articleData = res.data[0].articleId;
                  picPath = res.data[0].path;
                  articleData.picAlt = res.data[0].alt;
                }
                axios.get(`http://127.0.0.1:8000/picture/find/${picPath}`).then(res => {
                  articleData.qty = qty[articleData.id];
                  articleData.picData = res.data;
                  this.setState({
                    price: parseFloat(parseFloat(this.state.price) + (parseFloat(articleData.price) * parseFloat(articleData.qty)))
                  });
                  var result = this.state.result;
                  result = [result, this.setResult(articleData)];
                  this.setState({
                    result: result
                  });
                }).catch(err => {console.log(err)})
              }).catch(err => {console.log(err);})
            }).catch(err => {this.setState({result:"article does not exist anymore"})});
          }
          return true;
        });
      } else {  // si localStorage est vide
        this.setState({
          result: <p className="text-danger">Your basket is empty </p>
        });
      }
    }

    setResult = (info) => {
      var src =`data:image/jpeg;base64,${info.picData}`
      return (
        <div key={info.id} className="content-article">
          <div className="photo">
            <img src={src} alt={info.picAlt} />
          </div>
          <div className="titre">
            <div>
              <h3>{info.title}</h3>
            </div>
            <div>
              <label>Qty : </label>
              <input data-id={info.id} type="text" value={info.qty} disabled/>
            </div>
          </div>
          <div className="prix">{info.price} €</div>
          
        </div>
      );
    }

    displayBasket(){
      return this.state.result;
    }

    myTrim(x) {
      return x.replace(",",'');
    }

    countOccuranceInArray(array, value) {
      let occur = array.filter(arr => arr === value);
      return occur.length;
    }

    handleSumit(event){
        event.preventDefault();

        if(event.target.adresse.value == "" || event.target.cb.value == ""){
            this.setState({
                message: "You must complete all fields"
            })
        }else{
            let id = this.props.id;
            var data = {
                adresse: event.target.adresse.value,
                cb: event.target.cb.value,
                article: localStorage.getItem("id_product"),
                id: id,
            }
            let formData = new FormData();
            for (var [key, value] of Object.entries(data)) {
              formData.append(key, value);
            }
            axios.post(`http://localhost:8000/order/new`, formData).then(res => {
                document.getElementById('message').innerHTML = "Order saved !";
                document.getElementById('message').className = "alert alert-success";
                localStorage.setItem('id_product', "");
               this.setState({redirect: <Redirect to="/"/>});
            })
            .catch(err => {
                document.getElementById('message').innerHTML = "Article failed !";
                document.getElementById('message').className = "alert alert-danger";
            })
            }
    }

    render(){
      return(
        <div className="container">
            {this.state.redirect}
            <hr></hr>
            {this.displayBasket()}
            <hr></hr>
            <div className="total-article">
                <h5>Total price of items :</h5>
                <p>{this.state.price} €</p>
            </div>
            <hr></hr>
                <div>
                    <h5>Complete your command:</h5>
                    <p className="error text-danger">{this.state.message}</p>
                    <form onSubmit={(event) => this.handleSumit(event)} >
                        <div className="choiceUser">
                            <div> <AdresseBasket /></div>
                            <div> <CbBasket /></div>
                        </div>
                        <hr></hr>
                        <div className="add">
                          <a href="#/user/add/credit-card">Add a credit card</a>
                          <a href="#/user/edit/adress">Add a delivery adress</a>
                        </div>
                        <input className="btn btn-success" type="submit" value="Confirm" name="submit"/>
                    </form>
                </div>
        </div>
      )
    }
}
export default CartBasket;
