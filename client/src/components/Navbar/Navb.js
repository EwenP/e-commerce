/**************************/
/*      Nav bar, menu     */
/**************************/

import React from 'react';
import { Link } from 'react-router-dom';
import { Navbar, Nav, NavDropdown, Form, FormControl } from 'react-bootstrap';
import "./NavBar.css";
import { IoMdLogIn, IoIosCart, IoMdLogOut } from 'react-icons/io'
import { FaRegRegistered, FaSearch } from 'react-icons/fa'

class Navb extends React.Component {

  displayLoginOrLogout = () => {
    let log = (this.props.role.indexOf("ROLE_USER") !== -1) ?
      <Link className="nav-link" to="/logout"><IoMdLogOut size={25} color="blue" data-toggle="tooltip" title="Disconnect"/></Link> :
      <Link className="nav-link" to="/login"><IoMdLogIn size={25} color="blue" data-toggle="tooltip" title="Log In"/></Link>
    return log;
  }

  displayArticleMenu = () => {
    let admin = (this.props.role.indexOf("ROLE_ADMIN") !== -1) ?
      <NavDropdown.Item href="/#/admin/article/new">Add an article</NavDropdown.Item> :
      <NavDropdown.Item href="/">All articles</NavDropdown.Item>
    return admin;
  }

  displayProfilMenu = () => {
    let user = (this.props.role.indexOf("ROLE_USER") !== -1) ?
      <>
        <NavDropdown.Item href="/#/show">My Account</NavDropdown.Item>
        <NavDropdown.Item href="/#/user/add/credit-card">Payment</NavDropdown.Item>
        <NavDropdown.Item href="/#/user/edit/adress">Delivery</NavDropdown.Item>
      </> :
      <NavDropdown.Item href="/#/register">Create my account</NavDropdown.Item>
    return user;
  }

  render () {
    return (
      <div>
      <Navbar className="border-bottom bg-dark" expand="lg">
      <Navbar.Brand className="text-white font-weight-bold">
      <Link to="/"><img className="logo" src="/assets/logoWeb.png" alt="e-commerce"/></Link>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="navbar-toggle" className="border-0" />
      <Navbar.Collapse className="ml-5" id="navbar-toggle">
      <NavDropdown title="Profil" className="edit" id="basic-nav-dropdown">
      {this.displayProfilMenu()}
      </NavDropdown>
      <NavDropdown title="Products" id="basic-nav-dropdown">
      {this.displayArticleMenu()}
      </NavDropdown>

      <Form inline className="mx-auto">
      <FormControl type="text" placeholder="Find products here" className="xl-8 lg-8 md-3 sm-2 navbar mr-sm-2"/>
      <a href="/" variant="outline-success"><FaSearch size={20} color="white" /></a>
      </Form>

      <Nav className="ml-auto">
      {this.displayLoginOrLogout()}
      <Link className="nav-link" to="/register"><FaRegRegistered size={22} color="red" data-toggle="tooltip" title="Create account"/></Link>
      <Link className="nav-link" to="/cart"><IoIosCart size={25} color="orange" data-toggle="tooltip" title="See Orders"/></Link>
      </Nav>
      </Navbar.Collapse>
      </Navbar>
      <div id="message" data-target="false" className="alert gradient"><span className="font-montserra" >Welcome</span></div>
      </div>
    );
  }
}

export default Navb;
