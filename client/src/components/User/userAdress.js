import React, { Component } from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import auth from './../../Auth';

class EditUserAdress extends Component {
    constructor(props) {
        super(props);
        this.state = {
            adress: "",
            city: "",
            post_code: "",
            country: "",
            userId: null
        }
        Promise.all([auth()])
        .then(data => {
          let userId = data[0].id;
          this.setState({userId: userId});
        });
    }

    checkInput(data){
      if (data.adress.value === "" ||
      data.city.value === "" ||
      data.postcode.value === "" ||
      data.country.value === "") {
        alert("You must complete all mandatory fields");
        return false
      } else
        return true
    }

    handleSubmit(event){
        event.preventDefault();
        let formData = new FormData();
        if(this.checkInput(event.target)){
            let data = {
                adress: event.target.adress.value,
                city: event.target.city.value,
                postcode: event.target.postcode.value,
                country: event.target.country.value
            };
            for (var [key, value] of Object.entries(data)) {
                formData.append(key, value);
            }
            axios({
                url: `http://127.0.0.1:8000/user/${this.state.userId}/adress`,
                method: 'POST',
                headers: {
                    "Content-Type": "multipart/form-data",
                    "Authorization" : localStorage.getItem('token')
                },
                data: formData
            })
            .then(res => {
              document.getElementById('message').innerHTML = "New adress saved";
              document.getElementById('message').className = "alert alert-success";
            }).catch(err => {
              document.getElementById('message').innerHTML = "Add adress failed";
              document.getElementById('message').className = "alert alert-danger";
            });
        } else {
          return false;
        }
    }

    render(){
        return (
            <div>
                <div className="container font-montserra">
                <h1>Complete your personal infos</h1>
                  <div className="about col-6 mx-auto">
                    <form onSubmit={(event) => this.handleSubmit(event)} className="form">
                        <div className="form-group">
                            <label>
                                Adress:
                                <input className="form-control"
                                name="adress"
                                type="text"
                                />
                            </label>
                        </div>
                        <div className="form-group">
                            <label>
                                City:
                                <input className="form-control"
                                name="city"
                                type="text"
                                />
                            </label>
                        </div>
                        <div className="form-group">
                            <label>
                                Post code:
                                <input className="form-control"
                                name="postcode"
                                type="number"
                                minLength="5"
                                maxLength="7"
                                />
                            </label>
                        </div>
                        <div className="form-group">
                            <label>
                                Country:
                                <input className="form-control"
                                name="country"
                                type="text"/>
                            </label>
                        </div>
                        <input className="btn btn-primary"
                        type="submit"
                        value="Save"
                        />
                    </form>
                  </div>
                </div>
            </div>
        );
    }
}
export default EditUserAdress;
