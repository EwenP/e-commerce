import React from 'react';
import { Redirect } from 'react-router'
import axios from 'axios';
import EditUserInfos from './userEdit';
import "./userStyle.css";
import auth from './../../Auth';


class DisplayUserAdress extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    adress:[],
    userId: null
    }
    Promise.all([auth()])
    .then(data => {
        this.setState({userId: data[0].id});
        this.getUserInfo();
    });
  }

  getUserInfo() {
    axios.get(`http://localhost:8000/user/${this.state.userId}/getAdresse`)
    .then(res => {
        this.setState({adress: res.data})
    })
  }

  displayInfos(){
    var adresse = this.state.adress;
      var infos =
          adresse.map((data, i) => {
          return (
            <li key={i}>
              <b>Adress {i + 1}: </b>
              {data.adresse}
              <br/>
              {data.city}{" "}{data.postCode}{" "}{data.contry}
              <input className="deleteLogo" type="image" src="./assets/croix_rouge.png" onClick={() => this.deleteAdresse(data.id)}/>
            </li>)
          })
      return infos;
  }

  deleteAdresse(id){
    axios.delete(`http://localhost:8000/user/delete/adresse/${id}`,{
        headers: {
            Authorization: localStorage.getItem('token')
        }
    })
    .then(res => {
      this.getUserInfo()
    })
    .catch(err => {
        return false;
    })
  }

  render() {
    return (
      <div>
        {this.displayInfos()}
      </div>
    );
  }
}

export default DisplayUserAdress;
