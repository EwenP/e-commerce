import React from 'react';
import { Redirect } from 'react-router'
import axios from 'axios';
import EditUserInfos from './userEdit';
import "./userStyle.css";
import UserAdresse from "./DisplayUserAdress";
import UserCB from "./DisplayUserCB";

class DisplayUserInfos extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      modifier: false,
      redirection: false,
      id: this.props.id
    }
    console.log(this.props.id)
    this.getUserInfo()
  }

  getUserInfo() {
    axios.get(`http://localhost:8000/user/${this.props.id}`)
      .then(res => {
        this.setState({user: res.data});
      }).catch(err => {return false});
  }

  displayInfos(){
    if(!this.state.modifier) {
      var allInfos =
        <div className="container font-montserra">
            <h1>About you !</h1>
          <div className="about col-6 mx-auto">
            <ul>
            <h5>Personnal Info</h5>
              <hr/>
              <li><b>Firstname:</b>  {this.state.user.firstname}</li>
              <li><b>Lastname:</b> {this.state.user.lastname}</li>
              <li><b>Email:</b>  {this.state.user.email}</li>
              <li><b>Phone:</b> { this.state.user.phone}</li>
              <br/>
              <h5>Delivery Adress</h5>
              <hr/>
              {<UserAdresse />}
              <br/>
              <h5>Credit Card</h5>
              <hr/>
              {<UserCB />}
            </ul>
              <button onClick={() => this.change()} className="btn btn-success ml-4"> Edit </button>
              <button onClick={() => this.delete()} className="btn btn-danger ml-4"> Delete account</button>
          <div className="add">
            <a href="#/user/add/credit-card">Add a credit card</a>
            <a href="#/user/edit/adress">Add a delivery adress</a>
          </div>
          </div>
         </div>
      return allInfos;
    }
    else if (this.state.modifier) {
      return (
        <EditUserInfos
          change={() => this.change()}
          infos= {this.state.user}
          id={this.state.id}
        />
      );
    }
  }

  change() {
    this.state.modifier ? this.setState({modifier:false}) : this.setState({modifier:true})
    this.getUserInfo()
  }

  delete(){
    axios.delete(`http://localhost:8000/user/${this.props.id}`)
    .then(res => {
      this.setState({ redirection: true })
    }).catch(err => {return false});
  }

  render() {
    const { redirection } = this.state;
    if (redirection) {
     return <Redirect to='/'/>;
    }
    else{
      return (
        <div>
          {this.displayInfos()}
        </div>
      );
    }
  }
}
export default DisplayUserInfos;
