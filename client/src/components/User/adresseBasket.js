import React from 'react';
import axios from 'axios';
import "./userStyle.css";
import auth from './../../Auth';

class DisplayUserAdressBasket extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    adress:[], 
    userId: null
    }
    Promise.all([auth()])
    .then(data => {
        let userId = data[0].id;
        this.setState({userId: userId});
        this.getUserInfo();     
    });
}

getUserInfo() {
    axios.get(`http://localhost:8000/user/${this.state.userId}/getAdresse`)
    .then(res => {
        this.setState({adress: res.data})
    })
}

  displayInfos(){
    var adresse = this.state.adress;
      var infos =
          adresse.map((data, i) => {
          return (<li key={i}><b>Adress {i + 1}:</b> {data.adresse}<br/>{data.city}{" "}{data.postCode}{" "}{data.contry}<input className="radio" type="radio" name="adresse" value={data.id}/></li>)
          })
      return infos;
  }

  deleteAdresse(id){

    axios.delete(`http://localhost:8000/user/delete/adresse/${id}`,{
        headers: {
            Authorization: localStorage.getItem('token')
        }
    })
    .then(res => {
      console.log(res)
      this.getUserInfo()
    })
    .catch(err => {
        console.log(err)
    })
}

  render() {
      return (
        <div>
          {this.displayInfos()}
        </div>
      );
  }
}
export default DisplayUserAdressBasket;
