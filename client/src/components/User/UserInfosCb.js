import React, { Component } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router'
import 'bootstrap/dist/css/bootstrap.min.css';
import auth from './../../Auth';
class UserInfosCb extends Component {
    constructor(props) {
        super(props);
        this.state = {
            card_number: "",
            expiration_date: "",
            error:"",
            userId: null
        }
        // this.goBack = this.goBack.bind(this);
        Promise.all([auth()])
        .then(data => {
          let userId = data[0].id;
          this.setState({userId: userId});
        });
    }

    goBack = () => {
        this.props.history.goBack();
    }

    checkInput(data){
        if(data.card.value === "" ||
        data.owner.value === "" ||
        data.expiration.value === ""
        ){
          alert("You must complete all mandatory fields");
          return false
        }else
          return true
    };

    handleSubmit(event){
        event.preventDefault();
        let formData = new FormData();
        if(this.checkInput(event.target)){
            let data = {
                card: event.target.card.value,
                expiration: event.target.expiration.value + "-01"
            };
            for (var [key, value] of Object.entries(data)) {
                formData.append(key, value);
            }
            axios({
                url: `http://127.0.0.1:8000/user/${this.state.userId}/card`,
                method: 'POST',
                headers: {
                    "Content-Type": "multipart/form-data"
                },
                data: formData
            })
            .then(res => {
              document.getElementById('message').innerHTML = "New CB saved";
              document.getElementById('message').className = "alert alert-success";
            }).catch(err => {
                document.getElementById('message').innerHTML = "New CB saved";
                document.getElementById('message').className = "alert alert-success";
            });
        }else{
            return false;
        }
    }

    handleChange(event){
        if(event.target.type === "number"){
            let cardlength = event.target.value.length;
            if(cardlength < 16){
                this.setState({
                    card_number: event.target.value
                })
            }
            else{
                event.target.value = this.state.card_number
            }
        }
        else if(event.target.type === "month"){
            var date = new Date();
            var cardDate = new Date(event.target.value);
            var diffDate=(cardDate - date)
            if(diffDate < 2628000){
                this.setState({
                    error: "Expiration date invalid."
                })
            }else{
                this.setState({
                    error: ""
                })
            }
        }
    }

    render(){
        return (
            <div className="container font-montserra">

                <h2>Add a credit card</h2>
                <div className="about col-6 mx-auto">
                    <form onSubmit={(event) => this.handleSubmit(event)} className="form">
                        <div className="form-group">
                            <label>
                                Card number:
                                <input className="form-control"
                                name="card"
                                type="number"
                                onChange={(event) => this.handleChange(event)}
                                />
                            </label>
                        </div>
                        <div className="form-group">
                            <label>
                                Card's owner:
                                <input className="form-control"
                                name="owner"
                                type="text"
                                />
                            </label>
                        </div>
                        <div className="form-group">
                            <label>
                                Expiration date:
                                <input className="form-control"
                                name="expiration"
                                type="month"
                                onChange={(event) => this.handleChange(event)}
                                />
                            </label>
                            <p>{this.state.error}</p>
                        </div>
                        <input className="btn btn-primary"
                        type="submit"
                        value="Save"
                        />
                    </form>
                </div>

            </div>
        );
    }
}
export default UserInfosCb;
