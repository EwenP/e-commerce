import React from 'react';
import { Redirect } from 'react-router'
import axios from 'axios';
import EditUserInfos from './userEdit';
import "./userStyle.css";
import auth from './../../Auth';


class DisplayUserCbBasket extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    card:[], 
    userId: null
    }
    Promise.all([auth()])
    .then(data => {
        let userId = data[0].id;
        this.setState({userId: userId});
        this.getUserInfo();     
    });
}

getUserInfo() {
    axios.get(`http://localhost:8000/user/${this.state.userId}/getCB`)
    .then(res => {
        this.setState({card: res.data})
    })
}

  displayInfos(){
    var card = this.state.card;
    // console.log(card)
    var infos =
    card.map((data, i) => {
        var machaine= "" + data.cardNumber + "";
        // const last4Digits = machaine.slice(-4);
        const maskedNumber = machaine.slice(-4).padStart(machaine.length, '*');
        return(
            <li className="flex" key={i}>
                <b>Card {i+1}:</b>
                {maskedNumber}<input className="radio" type="radio" name="cb" value={data.id}/>
            </li>)
        }) 
      return infos;
  }

  deleteCard(id){
      axios.delete(`http://localhost:8000/user/delete/CB/${id}`,{
          headers: {
              Authorization: localStorage.getItem('token')
          }
      })
      .then(res => {
          this.getUserInfo();
      })
  }

  render() {
    //   console.log(this.state.card)
      return (
        <div>
          {this.displayInfos()}
        </div>
      );
  }
}
export default DisplayUserCbBasket;
