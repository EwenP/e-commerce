import React from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import "./userStyle.css";

class EditUserInfos extends React.Component {
  constructor(props) {
        super(props);
        this.state = {
            firstname:"",
            lastneme:"",
            email:"",
            phone:""
        }
    }

  checkInput(data){
    if(data.firstname.value === "" ||
    data.lastname.value === "" ||
    data.email.value === "" ||
    data.phone.value === ""
    ){
      alert("You must complete all mandatory fields");
      return false
    }
    else
      return true
  }

  handleSubmit(event){
    event.preventDefault();
    if(this.checkInput(event.target)){
      let data = {
        firstname: event.target.firstname.value,
        lastname: event.target.lastname.value,
        email: event.target.email.value,
        phone: event.target.phone.value,
        // password: event.target.password.value
      };
      axios.post(`http://127.0.0.1:8000/user/${this.props.id}/edit`, data)
      .then(res => {
        console.log(res.data);
        this.props.change();
      }).catch((err) => {
        console.log(err)
      });
    }
    else
      return false
  }
    render() {
      return (
        <div className="container font-montserra">
          <h1>Edit infos</h1>
          <div className="aboutinfo col-6 mx-auto">
            <form className="form" onSubmit={(event) => this.handleSubmit(event)}>
              <div class="form-group row">
                <label className="ml-5 mr-2">Firstname:*</label>
                <div>
                  <input type="text" name="firstname" className="col-10 justify-content-center form-control" defaultValue={this.props.infos.firstname}/>
                </div>
              </div>
              <div class="form-group row">
                <label className="ml-5 mr-2">Lastname:*</label>
                <div>
                  <input type="text" name="lastname" className="col-10 justify-content-center form-control" defaultValue={this.props.infos.lastname}/>
                </div>
              </div>
              <div class="form-group row">
                <label className="ml-5 mr-2">Email:*</label>
                <div>
                  <input type="email" name="email" className="col-10 justify-content-center form-control" defaultValue={this.props.infos.email}/>
                </div>
              </div>
              <div class="form-group row">
                <label className="ml-5 mr-2">Phone*:</label>
                <div>
                  <input type="number" name="phone" className="col-10 justify-content-center form-control" defaultValue={this.props.infos.phone}/>
                </div>
              </div>
              <input className="ml-4 btn btn-primary"type="submit" name="submit" value="Save"/>
            </form>
            <button className="btn btn-success ml-4" onClick={()=>{this.props.change()}}> Back </button>
          </div>
        </div>
    );
  }
}
export default EditUserInfos;
