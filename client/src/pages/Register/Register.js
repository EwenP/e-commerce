import React, { Component } from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { Redirect } from 'react-router';
import axios from 'axios';

class Register extends Component {

    state = {
        user: {
            lastname:'',
            firstname:'',
            phone:'',
            email:'',
            password:''
        },
        successMsg:'',
        formError:{
            lastname:'',
            firstname:'',
            email:'',
            phone:'',
            password:'',
            formEmpty:''
        },
        redirect:false
    }

    handleInputChange = (e) => {
        const input = e.target.name;
        const user = this.state.user;
        user[input] = e.target.value;
        // let formErrors = { ...this.state.formErrors };
        this.setState({ user });
    }

    handleFormSubmit = (e) => {
        e.preventDefault();
        // console.log('submitted')
        const lastname = this.state.user.lastname;
        const firstname = this.state.user.firstname;
        const phone = this.state.user.phone;
        const email = this.state.user.email;
        const password = this.state.user.password;
        const formData = { lastname, firstname, phone, email, password };

        axios({
            url: 'http://127.0.0.1:8000/register',
            method: 'POST',
            headers: {
                "Content-Type": "multipart/form-data",
            },
            data: formData
        }).then(res => {
            console.log(res)
            this.setState({ successMsg: res.data.msg, redirect: true })
        }).catch(err => {
            console.log(err)
        })
    }

    render(){
        const { redirect } = this.state;
        if (redirect) {
            return <Redirect to='/login' />
        }
        return(
            <Container fluid={true}>
                <Row className="vh-100 justify-content-center align-items-center">
                    <Col xl={4} lg={4} md={4} sm={4} className="p-5">
                        <h1 className="text-secondary text-center mb-5 font-righteous">Create an Account</h1>
                        <span>{this.state.successMsg}</span>
                        <Form onSubmit={this.handleFormSubmit} className="font-montserra">
                            <Form.Group>
                                <Form.Label>Lastname</Form.Label>
                                <Form.Control onChange={this.handleInputChange}  name="lastname" type="text" placeholder="Enter your lastname" style={{ borderRadius:50 }}/>
                                <Form.Text className="text-muted">
                                </Form.Text>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Firstname</Form.Label>
                                <Form.Control onChange={this.handleInputChange}  name="firstname" type="text" placeholder="Enter your firstname" style={{ borderRadius:50 }}/>
                                <Form.Text className="text-muted">
                                </Form.Text>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Phone Number</Form.Label>
                                <Form.Control onChange={this.handleInputChange}  name="phone" type="text" placeholder="Enter your phone number" style={{ borderRadius:50 }}/>
                                <Form.Text className="text-muted">
                                </Form.Text>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Email address</Form.Label>
                                <Form.Control onChange={this.handleInputChange}  name="email" type="email" placeholder="Enter email" style={{ borderRadius:50 }}/>
                                <Form.Text className="text-muted">
                                </Form.Text>
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control onChange={this.handleInputChange}  name="password" type="password" placeholder="Enter Password" style={{ borderRadius:50 }}/>
                            </Form.Group>
                            <div className="text-center mt-5">
                            <Button variant="danger" type="submit" style={{ borderRadius:50, width:200 }}>
                                Create
                            </Button>
                            </div>
                        </Form>
                    </Col>
                </Row>
            </Container>
        )
    }
}
export default Register;
