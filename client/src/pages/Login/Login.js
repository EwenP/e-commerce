import React, { Component } from 'react';
import { Redirect } from 'react-router';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import axios from 'axios';


class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                email:'',
                password:''
            },
            errors: {},
            redirect: false
         }
    }    
    // Handle Input when typing
    handleInputChange = (e) => {
        const input = e.target.name;
        const user = this.state.user;
        user[input] = e.target.value
        this.setState({ user });
    }

    handleFormSubmit = (e) => {
        e.preventDefault();
        let formData = new FormData();
        formData.append('email', this.state.user.email);
        formData.append('password', this.state.user.password);
        axios.post('http://127.0.0.1:8000/login', formData, { headers: { 'Accept': 'application/json'} })
        .then(( response ) => {
          if (response.data.success !== undefined) {
            document.getElementById('message').innerHTML = "Login faild, wrong email or password";
            document.getElementById('message').className = "alert alert-danger";
            localStorage.setItem('token', '');
          } else if (response.data !== '') {
            document.getElementById('message').innerHTML = "Welcome";
            document.getElementById('message').className = "alert alert-success";
            localStorage.setItem('token', response.data);
            this.setState({redirect: true})
          }
        })
        .catch( err => console.log(err));
    }

    render() {
        if (this.state.redirect) {
            return <Redirect to='/' />
        }
        return (
            <div className="login">            
            <Container fluid={true}>
                <Row className="vh-100 justify-content-center align-items-center">                    
                    <Col xl={4} lg={6} md={6} sm={6}>
                        <div className="">
                            <h1 className="mb-3 display-2 font-righteous text-secondary">Welcome To</h1>
                            <img src="/assets/logoWeb.png" width="45%" alt="logo"/>                            
                        </div>
                    </Col>
                    <Col xl={4} lg={6} md={6} sm={6} >
                        <div className="p-5" >
                            <h1 className="text-center  mb-5 font-righteous text-secondary">Log In </h1>
                            <Form onSubmit={this.handleFormSubmit}>
                                <Form.Group controlId="formBasicEmail">
                                    <Form.Label className="font-montserra">Email address</Form.Label>
                                    <Form.Control onChange={this.handleInputChange} name="email" type="email" placeholder="email@adress.com" style={{ borderRadius:50 }}/>
                                    <Form.Text className="text-muted">
                                    </Form.Text>
                                </Form.Group>
                                <Form.Group controlId="formBasicPassword">
                                    <Form.Label className="font-montserra">Password</Form.Label>
                                    <Form.Control onChange={this.handleInputChange} name="password" type="password" placeholder="Password" style={{ borderRadius:50 }}/>
                                </Form.Group>
                                <div className="text-center">
                                    <Button variant="outline-warning font-montserra" type="submit" style={{ borderRadius:50, width:200 }}>
                                    Log In
                                </Button>
                                </div>
                            </Form>
                        </div>
                    </Col>
                </Row>
            </Container>
            </div>
         );
    }
}

export default Login;
