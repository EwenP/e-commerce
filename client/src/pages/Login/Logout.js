import React, { Component } from 'react';
import { Redirect } from 'react-router';
import axios from 'axios';

class Logout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      logout: false
    }
  }

  componentDidMount() {
    localStorage.setItem('token', '');
    axios.get('http://127.0.0.1:8000/logout').then(res => {
      this.setState(({logout: true}));
    }).catch(err => {return false})
  }

  render() {
    return (
      <Redirect to="/login" />
    )
  }
}

export default Logout;
