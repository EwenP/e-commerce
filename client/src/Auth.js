import axios from 'axios';

function isAuth() {
  let token = localStorage.getItem('token');
  let auth = false;
  if (token !== '') {
    return (
      axios.get(`http://127.0.0.1:8000/auth/${token}`).then(res => {
        return (res.data);
      }).catch(err => {return false})
    );
  }
  return false;
}

export default isAuth;
