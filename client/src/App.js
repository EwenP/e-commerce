import React, {Component} from 'react';
import { Redirect } from 'react-router';
import { HashRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import ShowArticle from './components/Articles/ShowArticle.js';
import ArticleDelete from './components/Articles/ArticleDelete.js';
import VignetteArticle from './components/Articles/VignetteArticle.js';

import ArticleEdit from './components/Articles/ArticleEdit';
// import Specification from './components/Articles/Specification.js'


import { Container } from 'react-bootstrap'
import Navb from './components/Navbar/Navb';
import UserShow from './components/User/DisplayUserInfos';
import UserInfosCb from './components/User/UserInfosCb';
import EditUserAdress from './components/User/userAdress';
import Login from './pages/Login/Login';
import Logout from './pages/Login/Logout';
import Register from './pages/Register/Register';
import ArticleNew from './components/Articles/ArticleNew'
import Cart from './components/Cart/DisplayCart'
import Order from './components/Cart/order'
import auth from './Auth';

class App extends Component {
    constructor(props) {
      super(props);
      this.state = {
        articles:[],
        role: ["ROLE_VISITOR"],
        id: 0
      }
    }

    componentDidMount() {
      Promise.all([auth()])
      .then(data => {
        if (data[0].role !== undefined && data[0].id !== undefined)
          this.setState({role: data[0].role, id: data[0].id})
      });
    }

    addProductToBasket= (id) => {
        var key="id_product";
        var basket = localStorage.getItem(key);
        if(basket !== null && basket !== ""){
            basket = basket + ',' + id;
        } else {
            basket = id
        }
        localStorage.setItem(key, basket);
    }

    render(){
      let role = this.state.role;
        return (
          <HashRouter>
               <Navb role = {this.state.role}/>
               <Container fluid={true}>
                <Switch>
                   <Route
                       path="/"
                       exact
                       render={() => <VignetteArticle addProduct={this.addProductToBasket} role = {this.state.role} /> }
                   />
                   {this.state.route}
                   <Route
                       path="/show"
                       exact
                       render={ () => (role.indexOf("ROLE_USER") !== -1) ? <UserShow role = {this.state.role} id = {this.state.id}/> : <Redirect to='/login' />}
                   />
                   <Route
                       path="/user/edit/adress"
                       exact
                       render={ () => (role.indexOf("ROLE_USER") !== -1) ? <EditUserAdress role = {this.state.role} id= {this.state.id}/> : <Redirect to='/login' />}
                   />
                   <Route
                       path="/user/add/credit-card"
                       exact
                       render={ () => (role.indexOf("ROLE_USER") !== -1) ? <UserInfosCb role = {this.state.role}/> : <Redirect to='/login' />}
                   />
                    <Route
                        path="/article/edit/:id"
                        render={ () => <ArticleEdit role = {this.state.role}/> }

                   />
                   <Route
                     path="/article/delete/:id"
                     exact
                     render={ () => (role.indexOf("ROLE_ADMIN") !== -1) ? <ArticleDelete /*showArticle={this.showArticleHandler()}*/ role = {this.state.role}/> : <Redirect to='/' />}
                   />
                   <Route
                      path="/article/:id"
                      render={ () => <ShowArticle addProduct={this.addProductToBasket}/> }
                   />
                   <Route
                      path="/admin/article/new"
                      render={ () => (role.indexOf("ROLE_ADMIN") !== -1) ? <ArticleNew role = {this.state.role}/> : <Redirect to='/' /> }
                   />
                   <Route
                       path="/login"
                       exact
                       render={ () => <Login /> }
                   />
                   <Route
                       path="/logout"
                       exact
                       render={ () => <Logout /> }
                   />
                   <Route
                       path="/register"
                       exact
                       render={ () => <Register /> }
                   />
                   <Route
                      path="/cart"
                      exact
                      render={ () => <Cart basket={this.state.basket} /> }
                    />
                    <Route
                       path="/order"
                       exact
                       render={ () => <Order id={this.state.id} /> }
                   /> 
                    <Route
                    render={() => <h1>404</h1>}
                    />
                    </Switch>
               </Container>

           </HashRouter>
        );
    }
}

export default App;
