Welcome to my e-commerce project carried out as part of my training at Epitech.

Technologies used:

* Back: Symfony 4
* Front: ReactJs


=============================

**How to launch the project:**

=============================


Get the database file from "/server/bdd/e-commerce.sql"


Create an 'e-commerce' database and import the "e-commerce.sql" file


-In **server/e-commerce/.env**:

Change the configuration of the database in the .env file (username & password) **l.28**

-In **server/e-commerce/** :

`composer install`
`composer update`
`symfony server: start`


-In **client/**:

`npm install`
`npm start`

Admin ID:

* email: admin@admin.fr
* password: a


Enjoy !!

Ewen Pinson

* There are small bugs due to the deadline.